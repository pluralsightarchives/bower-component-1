import angular from 'angular';

import 'angular-ui-router';

import CoursesController from './components/courses/CoursesController';

import coursesView from './components/courses/courses.html!';

var coursesModule = angular.module('app.courses', [ 'ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) => {

    $stateProvider
        .state('courses', {
        url: "/courses",
        template : coursesView
    });
}
])
    .controller('CoursesController', CoursesController);

export default coursesModule;

